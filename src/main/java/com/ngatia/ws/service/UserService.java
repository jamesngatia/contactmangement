/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.service;

import com.ngatia.ws.model.User;

/**
 *
 * @author ngatia
 */
public interface UserService {
    User findByUsername(String username);
}
