/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.service;

import com.ngatia.ws.model.Contact;
import java.util.Collection;

/**
 *
 * @author ngatia
 */
public interface ContactService {
     Collection<Contact> findAll();
    
    Contact findOne(Long id);
    
    Contact create(Contact contact);
    
    Contact update(Contact contact);
    
    void delete(Long id);
}
