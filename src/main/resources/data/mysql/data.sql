
INSERT INTO `contacts` 
    (
        `id`, `firstname`, 
        `lastname`, `phone`, `email`, 
        `created_at`
    ) 
    VALUES 
    (
        NULL, 'John', 
        'Doe', '0788834234', 'john@gmail.com', 
        '2016-09-19 08:16:12'
    ), (NULL, 'Stephen', 
        'Kijabe', '07453423', 'Steve@gmail.com', 
    '2016-09-19 00:00:00'
);


INSERT INTO `users` 
    (
        `id`, `username`, 
        `password`, `enabled`, `credentialsexpired`, 
        `expired`,`locked`
    ) 
    VALUES 
    (
        NULL, 'user', 
        '$2a$08$T.HoEsQdACMV81X6xHley.aB4gq5xkX5wBv9.a8gW8kOQ1QNVnDU2', 
        true, false, 
        false,false
    ),
    (
        NULL, 'admin', 
        '$2a$08$AjV4nWJhfn3dNUFZG0l53ehrHHAQx6rxNTmlJvsuYlEbg8YJtssB6', 
        true, false, 
        false,false
    ),
    (
        NULL, 'adminuser', 
        '$2a$08$AjV4nWJhfn3dNUFZG0l53ehrHHAQx6rxNTmlJvsuYlEbg8YJtssB6', 
        true, false, 
        false,false
    );

INSERT INTO `user_role` 
    (
        `user_id`, `role_id`
    ) 
    VALUES 
    (
        1, 2
    ),
    (
        2, 1
    ),
    (
        3, 1
    ),
    (
        3, 2
    );

INSERT INTO `roles`
    (
        `id`,`label`
    )
    VALUES
    (
        NULL,'ROLE_Admin'
    ),
    (
        NULL,'ROLE_User'
    );