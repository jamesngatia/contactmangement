///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.ngatia.ws.api;
//
//import com.ngatia.security.JwtTokenUtil;
//import com.ngatia.security.JwtUser;
//import javax.servlet.http.HttpServletRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// *
// * @author ngatia
// */
//@RestController
//public class UserController {
//    @Value("${jwt.header}")
//    private String tokenHeader;
//    
//    @Autowired
//    private JwtTokenUtil jwtTokenUtil;
//    
//    @Autowired
//    private UserDetailsService userDetailsService;
//    
//    @RequestMapping(value = "user", method = RequestMethod.GET)
//    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
//        String headertoken = request.getHeader(tokenHeader);
//        String token=headertoken.substring(7);
//        String username = jwtTokenUtil.getUsernameFromToken(token);
//        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
//        return user;
//    }
//}
