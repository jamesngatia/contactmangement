/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ngatia
 * Created: Sep 18, 2016
 */

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
    `id` BIGINT NOT NULL AUTO_INCREMENT , 
    `firstname` VARCHAR(50) NOT NULL , 
    `lastname` VARCHAR(50) NOT NULL , 
    `phone` VARCHAR(50) NOT NULL , 
    `email` VARCHAR(50) NOT NULL , 
    `created_at` DATETIME NOT NULL , 
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `contactsDB`.`users` 
    (
        `id` BIGINT NOT NULL AUTO_INCREMENT , 
        `username` VARCHAR(50) NOT NULL , 
        `password` VARCHAR(80) NOT NULL , 
        `enabled` BOOLEAN NOT NULL , 
        `credentialsexpired` BOOLEAN NOT NULL , 
        `expired` BOOLEAN NOT NULL , 
        `locked` BOOLEAN NOT NULL , 
        PRIMARY KEY (`id`), 
        UNIQUE `UNQ_username` (`username`)
    ) ENGINE = InnoDB;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `contactsDB`.`roles` ( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `label` VARCHAR(30) NOT NULL , 
    PRIMARY KEY (`id`)) ENGINE = InnoDB;


DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `contactsDB`.`user_role` ( 
        `user_id` INT NOT NULL , 
        `role_id` INT NOT NULL , 
        PRIMARY KEY (`user_id`, `role_id`)
    ) ENGINE = InnoDB;