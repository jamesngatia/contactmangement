/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.logger;

import com.ngatia.ws.model.Contact;
import com.ngatia.ws.service.ContactService;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author ngatia
 */

@Component
public class ContactLogger {
    
    private Logger logger=LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ContactService contactService;
    
    
    @Scheduled(cron="0,30 * * * * *")
    public void cronJob(){
        logger.info("> cronJob");
        
        Collection<Contact> contacts=contactService.findAll();
        logger.info("There are {} contacts in the data store", contacts.size());
        
        logger.info("< cronJob");
    }
}
