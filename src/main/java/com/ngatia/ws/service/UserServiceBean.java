/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.service;

import com.ngatia.ws.model.User;
import com.ngatia.ws.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ngatia
 */
@Service
public class UserServiceBean implements UserService{
    
    @Autowired
    private UserRepository userRepositoty;
            
            
    @Override
    public User findByUsername(String username) {
        User user=userRepositoty.findByUsername(username);
        return user;
    }
    
}
