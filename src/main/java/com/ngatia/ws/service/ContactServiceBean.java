/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.service;

import com.ngatia.ws.model.Contact;
import com.ngatia.ws.repository.ContactRepository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ngatia
 */

@Service
public class ContactServiceBean  implements ContactService{
    
    @Autowired
    private ContactRepository contactRepository;
    
    @Override
    public Collection<Contact> findAll() {
        Collection<Contact> contacts=contactRepository.findAll();
        return contacts;
    }

    @Override
    public Contact findOne(Long id) {
        Contact contact=contactRepository.findOne(id);
        return contact;
    }

    @Override
    public Contact create(Contact contact) {
        if(contact.getId()!=null){
            return null;
        }
        Contact savedContact=contactRepository.save(contact);
        return savedContact;
    }

    @Override
    public Contact update(Contact contact) {
        Contact contactParam=contactRepository.findOne(contact.getId());
        if(contact.getId()==null){
            return null;
        }
        Contact updatedContact=contactRepository.save(contact);
        return updatedContact;
    }

    @Override
    public void delete(Long id) {
        contactRepository.delete(id);
    }
    
}
