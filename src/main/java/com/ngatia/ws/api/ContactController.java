/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngatia.ws.api;

import com.ngatia.ws.model.Contact;
import com.ngatia.ws.service.ContactService;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ngatia
 */

@RestController
public class ContactController {
    @Autowired
    private ContactService contactService;
    
    @RequestMapping(
            value = "/api/contacts",
            method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Contact>> getContacts(){
        Collection<Contact> contacts=contactService.findAll();
        return new ResponseEntity<Collection<Contact>>(contacts,HttpStatus.OK);
    }
    
    
    @RequestMapping(
            value="/api/contacts/{id}",
            method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Contact> getContact(@PathVariable("id") Long id){
        Contact contact=contactService.findOne(id);
        if(contact==null){
            return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
        } 
        return new ResponseEntity<Contact>(contact,HttpStatus.OK);
    }
    
    @RequestMapping(
            value="/api/contacts",
            method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact){
        Contact savedContact=contactService.create(contact);
        return new ResponseEntity<Contact>(savedContact,HttpStatus.CREATED);
    }
    
    
    @RequestMapping(
            value="/api/contacts/{id}",
            method=RequestMethod.PUT,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Contact> updateContact(@RequestBody Contact contact){
        Contact updatedContact=contactService.update(contact);
        if(updatedContact==null){
            return new ResponseEntity<Contact>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Contact>(updatedContact,HttpStatus.OK);
    }
    
    @RequestMapping(
            value="/api/contacts/{id}",
            method=RequestMethod.DELETE
    )
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Contact> deleteContact(@PathVariable("id") Long id){
        
        contactService.delete(id);
        return new ResponseEntity<Contact>(HttpStatus.NO_CONTENT);
    }
}
