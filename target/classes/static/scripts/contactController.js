(function() {

	'use strict';

	angular
		.module('authApp')
		.controller('ContactController', ContactController);

	function ContactController($http, $auth, $rootScope) {

		var vm = this;

		vm.users;
		vm.error;
                
                $http.get('api/contacts').success(function(contacts) {
                        vm.contacts = contacts;
                        console.log(contacts);
                }).error(function(error) {
                        vm.error = error;
                });
                
		
                vm.delete=function(id,index){
                    if (confirm('Are you sure you want to delete these items?')){
                        $http({
                            method:'DELETE',
                            url:'api/contacts/'+id
                        }).success(function(){
                            vm.contacts.splice(index, 1);
                        }).error(function(error){
                            vm.delerror=error.message;
                        });
                    }
                    
                };

		// We would normally put the logout method in the same
		// spot as the login method, ideally extracted out into
		// a service. For this simpler example we'll leave it here
		vm.logout = function() {

			$auth.logout().then(function() {

				// Remove the authenticated user from local storage
				localStorage.removeItem('username');
				localStorage.removeItem('roles');

				// Flip authenticated to false so that we no longer
				// show UI elements dependant on the user being logged in
				$rootScope.authenticated = false;

				// Remove the current user info from rootscope
				$rootScope.currentUser = null;
			});
		}
	}
	
})();